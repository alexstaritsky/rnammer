#!/bin/bash
#============================================================================
#title          :SAPP RNAmmer
#description    :rRNA prediction using RNAmmer
#author         :Jasper Koehorst
#date           :2017
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test
fi

cp $DIR/build/libs/*jar $DIR/

java -jar $DIR/rnammer.jar --help