package nl.wur.ssb.rnammer;

import junit.framework.TestCase;

import java.io.File;

/**
 * Unit test for simple App.
 */
public class RNAmmerTest extends TestCase {

    public void testHelp() throws Exception {
        if (!new File("/.dockerenv").exists()) {
            App.main(new String[0]);
        }
    }


    public void testApp() throws Exception {
        String input = "./src/test/resources/input/example.hdt";
        File output = new File("./src/test/resources/output/example.hdt");
        output.delete();
        String[] args = {"-input", input, "-output", output.getAbsolutePath()};
        App.main(args);
    }

    // Only for local testing
    public void testEndpoint() throws Exception {
        String endpoint = "http://10.117.11.77:7200/repositories/diana";
        String update = "http://10.117.11.77:7200/repositories/Test/statements";
        String output = "endpoint_output.nt.gz";
        String[] args = {"-endpoint", endpoint, "-update", update, "-output", output };
        if (!new File("/.dockerenv").exists()) {
            App.main(args);
        }
    }
}
