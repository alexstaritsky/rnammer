package nl.wur.ssb.rnammer;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.FASTA;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.SappGeneric.Store;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Rnammer extends SequenceBuilder {

    private CommandOptions arguments;
    public final Logger logger = Logger.getLogger(Logger.class);

    public Rnammer(String[] args) throws Exception {
        super(null, "");
        arguments = new CommandOptions(args);
        this.domain = arguments.domain;

        Generic.Logger(arguments.debug);

        // Prepare software
        copy();

        // Generate fasta files
        List<File> files = FASTA.create(arguments, "genome");

        rnammerCaller(files);
        rrnaParser(files);
        save();
        // Cleaning
        for (File fastaFile : files) {
            new File(fastaFile + "_gff.rnammer").deleteOnExit();
            new File(fastaFile + "_fasta.rnammer").deleteOnExit();
            new File(fastaFile + "_rnammer.sh").deleteOnExit();
            fastaFile.deleteOnExit();
        }
    }

    private void copy() throws IOException {
        String path = Util.getTempFile("");
        String rnammer = Util.prepareFileFromJar("binaries/{OS}/rnammer-1.2.src.tar.gz");

        if (new File(path + "/rnammer-1.2.src").isDirectory()) {
            logger.info("Already here: " + path + "/rnammer-1.2.src");
            return;
        }
        new ExecCommand("tar -xvf " + rnammer + " -C " + path);
    }

    private void save() throws Exception {
        if (arguments.input != null) {
            // Converting output file to HDT
            Save.toHDT(arguments.output, arguments.output);
            logger.info("Results are stored in " + arguments.output + " as HDT");
        } else {
            logger.info("Results are stored in " + arguments.output + " as ntriples.gz");
        }
    }

    private void rrnaParser(List<File> files) throws Exception {
        for (File fastaFile : files) {
            Map<String, String> geneMap = new HashMap<>();
            Scanner genes = new Scanner(new FileReader(fastaFile + "_fasta.rnammer"));
            String sequence = "";
            String header = null;
            while (genes.hasNext()) {
                String line = genes.nextLine();
                if (line.startsWith(">")) {
                    header = line.trim().toLowerCase();
                    sequence = "";
                    geneMap.put(header, "");
                } else {
                    sequence += line.trim();
                    geneMap.put(header, sequence);
                }
            }
            genes.close();

            Scanner gff = new Scanner(new FileReader(fastaFile + "_gff.rnammer"));
            while (gff.hasNext()) {
                String line = gff.nextLine();

                if (!line.startsWith("#")) {
                    String[] lineSplit = line.split("\t+");
                    String dnaObject = lineSplit[0];
                    arguments.toolversion = lineSplit[1];
                    String type = lineSplit[2].toLowerCase();
                    int begin = Integer.parseInt(lineSplit[3]);
                    int end = Integer.parseInt(lineSplit[4]);
                    double score = Double.parseDouble(lineSplit[5]);
                    String strand = lineSplit[6];
                    String attribute = lineSplit[8];
                    String key = ">" + type + "_" + dnaObject + "_" + begin + "-" + end + "_DIR" + strand + " /molecule=" + attribute + " /score=" + score;

                    sequence = geneMap.get(key.toLowerCase());

                    String sha384 = Generic.checksum(sequence, "SHA-384");

                    life.gbol.domain.NASequence dnaobject;

                    String typeURI = null;
                    if (arguments.endpoint != null) {
                        typeURI = nl.wur.ssb.SappGeneric.SPARQL.getType(dnaObject, new Domain(arguments.endpoint).getRDFSimpleCon());
                    } else if (arguments.hdt != null) {
                        typeURI = nl.wur.ssb.SappGeneric.SPARQL.getType(dnaObject, arguments.hdt);
                    }


                    if (typeURI.contains("Scaffold")) {
                        dnaobject = this.domain.make(life.gbol.domain.Scaffold.class, dnaObject);
                    } else if (typeURI.contains("Contig")) {
                        dnaobject = this.domain.make(life.gbol.domain.Contig.class, dnaObject);
                    } else if (typeURI.contains("Chromosome")) {
                        dnaobject = this.domain.make(life.gbol.domain.Chromosome.class, dnaObject);
                    } else {
                        throw new Exception("Sequence type not known: " + typeURI);
                    }

                    this.featureURI = dnaobject.getResource().getURI();

                    Gene gene = this.domain.make(life.gbol.domain.Gene.class, dnaobject.getResource().getURI() + "/gene/" + begin + "_" + end);

                    rRNA rrna = this.domain.make(life.gbol.domain.rRNA.class, dnaobject.getResource().getURI() + "/rrna/" + begin + "_" + end);

                    String sampleid = gene.getResource().getURI().replace("http://gbol.life/0.1/", "").replaceAll("/.*", "");

                    Region region = this.makeRegion(begin, end, "rrna", dnaobject);
                    gene.setLocation(region);

                    // Creation of exon and exonlist...
                    Exon exon = this.domain.make(life.gbol.domain.Exon.class, dnaobject.getResource().getURI() + "/exon/" + begin + "_" + end);

                    exon.setLocation(region);

                    // Setting other features
                    rrna.setSequence(sequence);
                    rrna.setLength((long) sequence.length());
                    rrna.setSha384(sha384);


                    // TODO create a gene prov and a rrna prov

                    nl.systemsbiology.semantics.sapp.domain.RNAmmer rnaprov = this.domain.make(nl.systemsbiology.semantics.sapp.domain.RNAmmer.class, rrna.getResource().getURI() + "/" + arguments.tool + "/" + arguments.toolversion + "/prov");

                    life.gbol.domain.FeatureProvenance genefeatprov = this.domain.make(life.gbol.domain.FeatureProvenance.class, gene.getResource().getURI() + "/" + arguments.tool + "/" + arguments.toolversion + "/featureProv");

                    AnnotationResult annotResult = domain.make(AnnotationResult.class, arguments.annotResultIRI);

                    domain.disableCheck();
                    genefeatprov.setOrigin(annotResult);
                    domain.enableCheck();

                    rrna.setProduct(attribute);
                    rnaprov.setAttribute(attribute);
                    rnaprov.setScore(score);
                    genefeatprov.setAnnotation(rnaprov);

                    exon.addProvenance(genefeatprov);
                    ExonList exonList = domain.make(life.gbol.domain.ExonList.class, dnaobject.getResource().getURI() + "/exonList/" + begin + "_" + end);

                    exonList.addExon(exon);

                    rrna.setExonList(exonList);

                    gene.addProvenance(genefeatprov);

                    dnaobject.addFeature(gene);
                    gene.addTranscript(rrna);
                }
            }
            gff.close();
            // Save to file
            arguments.domain = domain;
            Save.save(arguments);
            domain.closeAndDelete();
            domain = Store.createDiskStore();
            arguments.domain = domain;
        }
    }

    private void rnammerCaller(List<File> files) throws Exception {
        for (File fastaFile : files) {
            String cmd = Util.getTempFile("") + "/rnammer-1.2.src/rnammer -S bac -m lsu,tsu,ssu -gff " + fastaFile + "_gff.rnammer -f " + fastaFile + "_fasta.rnammer -multi - < " + fastaFile;

            logger.info(cmd);

            PrintWriter writer = new PrintWriter(fastaFile + "_rnammer.sh", "UTF-8");
            writer.println(cmd);
            writer.close();

            if (!new File(fastaFile + "_gff.rnammer").exists()) {
                logger.info("Executing: " + fastaFile + "_rnammer.sh");

                ExecCommand code = new ExecCommand("bash " + fastaFile + "_rnammer.sh");
                if (code.getExit() > 0) {
                    logger.error(code.getOutput());
                    throw new Exception("Execution failed " + code.getError());
                }
            } else {
                logger.info(fastaFile + "_gff.rnammer already exists... skipping calculation");
            }
        }
    }
}
