package nl.wur.ssb.rnammer;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import nl.wur.ssb.SappGeneric.ImportProv;
import org.apache.commons.lang.StringUtils;
import org.rdfhdt.hdt.hdt.HDTManager;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {

    public String annotResultIRI;
    private String commandLine;
    final String sapp = "SAPP RNAmmer";
    private final String repository = "https://gitlab.com/sapp/GeneticElements/rnammer";
    final String description = "RNA prediction module from SAPP";
    private final LocalDateTime starttime = LocalDateTime.now();
    final String sappversion =
            nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[0];
    final String sappcommit =
            nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[1];
    AnnotationResult annotResult;

    final String tool = "RNAmmer";
    public String toolversion = "";


    public int locus = 1;

    CommandOptions(String args[]) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            // TODO make this default in all modules
            // Setting the AnnotationProvenance
            String[] files = new String[0];

            if (this.input != null) {
                files = new String[]{this.input.getAbsolutePath()};
                this.hdt = HDTManager.loadIndexedHDT(this.input.getAbsolutePath());
            }

            if (this.endpoint != null) {
                files = new String[]{this.endpoint};
                this.domain = new Domain(this.endpoint);
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            }


            ImportProv origin = new nl.wur.ssb.SappGeneric.ImportProv(domain, Arrays.asList(files), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

            if (this.help) {
                throw new ParameterException("");
            }

            if (this.input == null && this.endpoint == null) {
                throw new ParameterException("Required either -input or -endpoint");
            }

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}

