#RNAmmer SAPP module

RNAmmer rRNA prediction module
More information can be found [here](https://sapp.gitlab.io/modules/geneticelements/rrna/)

**Current module status:**

-------
### RNAmmer Master status

RNAmmer master [![build](https://gitlab.com/sapp/GeneticElements/rnammer/badges/master/build.svg)](https://gitlab.com/sapp/GeneticElements/rnammer/tree/master)

### RNAmmer dev status

RNAmmer dev [![build](https://gitlab.com/sapp/GeneticElements/rnammer/badges/dev/build.svg)](https://gitlab.com/sapp/GeneticElements/rnammer/tree/dev)
